@file:Suppress("DEPRECATION")

package com.byagowi.persiancalendar.utils

import com.byagowi.persiancalendar.PERSIAN_DIGITS

fun formatNumber(number: Int, digits: CharArray = PERSIAN_DIGITS): String =
    formatNumber(number.toString(), digits)

fun formatNumber(number: String, digits: CharArray = PERSIAN_DIGITS): String {
    return number.map { digits.getOrNull(Character.getNumericValue(it)) ?: it }
        .joinToString("")
}

fun <T> listOf12Items(
    x1: T, x2: T, x3: T, x4: T, x5: T, x6: T, x7: T, x8: T, x9: T, x10: T, x11: T, x12: T
) = listOf(x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12)

fun <T> listOf7Items(
    x1: T, x2: T, x3: T, x4: T, x5: T, x6: T, x7: T
) = listOf(x1, x2, x3, x4, x5, x6, x7)
