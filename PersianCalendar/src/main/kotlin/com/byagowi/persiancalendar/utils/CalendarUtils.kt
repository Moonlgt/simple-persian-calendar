package com.byagowi.persiancalendar.utils

import android.content.Context
import com.byagowi.persiancalendar.PERSIAN_MONTHS
import com.byagowi.persiancalendar.R
import com.byagowi.persiancalendar.WEEK_DAYS
import com.byagowi.persiancalendar.WEEK_DAYS_INITIALS
import com.byagowi.persiancalendar.entities.CalendarType
import com.byagowi.persiancalendar.entities.Jdn
import com.byagowi.persiancalendar.isTalkBackEnabled
import com.byagowi.persiancalendar.language
import com.byagowi.persiancalendar.spacedComma
import com.byagowi.persiancalendar.ui.blucalendar.PersianCalendar
import com.byagowi.persiancalendar.weekStartOffset
import java.util.*

fun applyWeekStartOffsetToWeekDay(dayOfWeek: Int): Int = (dayOfWeek + 7 - weekStartOffset) % 7

fun revertWeekStartOffsetFromWeekDay(dayOfWeek: Int): Int = (dayOfWeek + weekStartOffset) % 7

fun getWeekDayName(position: Int) = WEEK_DAYS[position % 7]

fun dayTitleSummary(jdn: Jdn, date: PersianCalendar): String =
    jdn.dayOfWeekName + spacedComma + formatDate(date)

fun getInitialOfWeekDay(position: Int) = WEEK_DAYS_INITIALS[position % 7]

// Generating text used in TalkBack / Voice Assistant
fun getA11yDaySummary(
    context: Context,
    jdn: Jdn,
    isToday: Boolean,
    withTitle: Boolean
): String = buildString {
    // It has some expensive calculations, lets not do that when not needed
    if (!isTalkBackEnabled) return@buildString

    if (isToday) appendLine(context.getString(R.string.today))

    val mainDate = jdn.toCalendar()

    if (withTitle) appendLine().append(dayTitleSummary(jdn, mainDate))
}

fun Date.toJavaCalendar(): Calendar =
    Calendar.getInstance().also {
        it.time = this
    }

//fun calculateDatePartsDifference(
//    higher: AbstractDate, lower: AbstractDate, calendar: CalendarType
//): Triple<Int, Int, Int> {
//    var y = higher.year - lower.year
//    var m = higher.month - lower.month
//    var d = higher.dayOfMonth - lower.dayOfMonth
//    if (d < 0) {
//        m--
//        d += calendar.getMonthLength(lower.year, lower.month)
//    }
//    if (m < 0) {
//        y--
//        m += calendar.getYearMonths(lower.year)
//    }
//    return Triple(y, m, d)
//}

fun formatDate(
    date: PersianCalendar
): String =
    language.dmy.format(
        formatNumber(date.persianDay),
        PERSIAN_MONTHS,
        formatNumber(date.persianYear)
    )

fun PersianCalendar.monthsDistanceTo(toDate: PersianCalendar) =
    (toDate.persianYear - this.persianYear) * 12 + toDate.getPersianMonth() - this.getPersianMonth()
