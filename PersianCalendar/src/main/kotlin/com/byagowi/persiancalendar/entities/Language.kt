package com.byagowi.persiancalendar.entities

import android.annotation.SuppressLint
import java.util.*

enum class Language(val code: String, val nativeName: String) {

    // The following order is used for language change dialog also
    // Official languages
    FA("fa", "فارسی"),
    FA_AF("fa-AF", "دری"),

    // Rest, sorted by their language code
    EN_US("en-US", "English");

    val language get() = code.replace(Regex("-(IR|AF|US|CN)"), "")

    val dmy: String
        get() = "%1\$s %2\$s %3\$s"

    val my: String
        get() = "%1\$s %2\$s"

    val defaultWeekStart
        get() = "0"

    companion object {
        @SuppressLint("ConstantLocale")
        val userDeviceLanguage = Locale.getDefault().language ?: "en"

        @SuppressLint("ConstantLocale")
        private val userDeviceCountry = Locale.getDefault().country ?: "IR"

        // Preferred app language for certain locale
        val preferredDefaultLanguage
            get() = when (userDeviceLanguage) {
                FA.code, "en", EN_US.code -> if (userDeviceCountry == "AF") FA_AF else FA
                else -> valueOfLanguageCode(userDeviceLanguage) ?: EN_US
            }

        fun valueOfLanguageCode(languageCode: String): Language? =
            values().find { it.code == languageCode }
    }
}
