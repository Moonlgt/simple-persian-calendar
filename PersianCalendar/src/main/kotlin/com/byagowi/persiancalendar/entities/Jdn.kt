package com.byagowi.persiancalendar.entities

import com.byagowi.persiancalendar.WEEK_DAYS
import com.byagowi.persiancalendar.ui.blucalendar.PersianCalendar
import com.byagowi.persiancalendar.ui.blucalendar.PersianDate
import com.byagowi.persiancalendar.utils.applyWeekStartOffsetToWeekDay
import com.byagowi.persiancalendar.utils.toJavaCalendar
import com.byagowi.persiancalendar.variants.debugLog
import com.byagowi.persiancalendar.weekEnds
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.ceil
import kotlin.math.roundToInt

// Julian day number, basically a day counter starting from some day in concept
// https://en.wikipedia.org/wiki/Julian_day
@JvmInline
value class Jdn(val value: Long) {

    constructor(persianCalendar: PersianCalendar) : this(persianCalendar.timeInMillis)
    constructor(persianDate: PersianDate) :
            this(
                PersianCalendar().apply {
                    setPersianDate(persianDate.yyyy, persianDate.month, persianDate.day)
                }
            )

    constructor(year: Int, month: Int, day: Int) :
            this(
                PersianCalendar().apply {
                    setPersianDate(year, month, day)
                }
            )

    // 0 means Saturday in it, see #`test day of week from jdn`() in the testsuite
    val dayOfWeek: Int
        get() =
            ((millisToDays(value) - 1 ) % 7L).toInt()

    fun isWeekEnd(): Boolean {
        val isWeekend = weekEnds[dayOfWeek]

        val ss = dayOfWeek
        val year = toCalendar().persianYear
        val month = toCalendar().getPersianMonth()
        val day = toCalendar().persianDay


        debugLog("value $value")
        debugLog("dayOfWeek $ss")
        debugLog("isWeekend $isWeekend")
        debugLog("year $year, month $month, day $day")

        return isWeekend
    }

    fun toCalendar() = PersianCalendar().apply { timeInMillis = value }

    fun createMonthDaysList(monthLength: Int) =
        (0 until monthLength)
            .map { dayOffset ->
                Jdn(value + dayToMillis(dayOffset - 1))
            }


    private fun dayToMillis(day: Int) =
        TimeUnit.DAYS.toMillis(day.toLong())

    private fun millisToDays(millis: Long) =
        (millis / (1000.0 * 60 * 60 * 24))

    operator fun compareTo(other: Jdn) = value.compareTo(other.value)
    operator fun plus(other: Int): Jdn = Jdn(value + other)
    operator fun minus(other: Int): Jdn = Jdn(value - other)

    // Difference of two Jdn values in days
    operator fun minus(other: Jdn): Int {
        val millionSeconds = value - other.value
        return millisToDays(millionSeconds).roundToInt()
    }

    fun getWeekOfYear(startOfYear: Jdn): Int {
        val dayOfYear = this - startOfYear
        return ceil(1 + (dayOfYear - applyWeekStartOffsetToWeekDay(this.dayOfWeek)) / 7.0).toInt()
    }

    val dayOfWeekName: String get() = WEEK_DAYS[this.dayOfWeek]

//    fun calculatePersianSeasonPassedDaysAndCount(): Pair<Int, Int> {
//        val persianDate = this.toPersianCalendar()
//        val season = (persianDate.month - 1) / 3
//        val seasonBeginning = PersianDate(persianDate.year, season * 3 + 1, 1)
//        val seasonBeginningJdn = Jdn(seasonBeginning)
//        return this - seasonBeginningJdn + 1 to
//                Jdn(seasonBeginning.monthStartOfMonthsDistance(3)) - seasonBeginningJdn
//    }

    companion object {
        fun today() = Jdn(Date().toJavaCalendar().timeInMillis)
    }
}
