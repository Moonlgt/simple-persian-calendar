package com.byagowi.persiancalendar.entities

import androidx.annotation.StringRes
import com.byagowi.persiancalendar.PERSIAN_DIGITS
import com.byagowi.persiancalendar.R
import com.byagowi.persiancalendar.ui.blucalendar.PersianCalendar
import com.byagowi.persiancalendar.utils.monthsDistanceTo

enum class CalendarType(
    @StringRes val title: Int,
    @StringRes val shortTitle: Int,
    val preferredDigits: CharArray
) {
    // So vital, don't ever change names of these
    SHAMSI(
        R.string.shamsi_calendar, R.string.shamsi_calendar_short, PERSIAN_DIGITS
    );

    // 1 means Saturday on it and 7 means Friday
    fun getLastWeekDayOfMonth(year: Int, month: Int, dayOfWeek: Int): Int {
        val monthLength = getMonthLength(year, month)
        return monthLength - (Jdn(year, month, monthLength) - dayOfWeek + 1).dayOfWeek
    }

    fun getYearMonths(year: Int) =
        (Jdn(year + 1, 1, 1) - 1).toCalendar().getPersianMonth()

    fun getMonthLength(year: Int, month: Int) =
        Jdn(getMonthStartFromMonthsDistance(year, month, 1)) - Jdn(year, month, 1)

    private fun getMonthStartFromMonthsDistance(
        baseYear: Int,
        baseMonth: Int,
        monthsDistance: Int
    ) =
        PersianCalendar().apply {
            var month: Int =
                monthsDistance + baseMonth - 1 // make it zero based for easier calculations

            var year: Int = baseYear + month / 12
            month %= 12
            if (month < 0) {
                year -= 1
                month += 12
            }
            setPersianDate(year, month + 1, 1)
        }

    fun getMonthsDistance(baseJdn: Jdn, toJdn: Jdn): Int =
        baseJdn.toCalendar().monthsDistanceTo(toJdn.toCalendar())

    fun getMonthStartFromMonthsDistance(baseJdn: Jdn, monthsDistance: Int): PersianCalendar {
        val date = baseJdn.toCalendar()
        return getMonthStartFromMonthsDistance(
            date.persianYear,
            date.getPersianMonth(),
            monthsDistance
        )
    }
}
