package com.byagowi.persiancalendar

import android.content.Context
import android.view.accessibility.AccessibilityManager
import androidx.core.content.getSystemService
import com.byagowi.persiancalendar.entities.CalendarType
import com.byagowi.persiancalendar.entities.Language
import com.byagowi.persiancalendar.utils.listOf12Items
import com.byagowi.persiancalendar.utils.listOf7Items
import com.byagowi.persiancalendar.variants.debugLog

const val LOG_TAG = "PersianCalendar"

val PERSIAN_MONTHS = listOf12Items(
    "فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد",
    "شهریور", "مهر", "آبان", "آذر", "دی",
    "بهمن", "اسفند"
)

val PERSIAN_DIGITS = charArrayOf('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹')

val WEEK_DAYS = listOf7Items(
    "شنبه", "یکشنبه", "دوشنبه", "سه‌شنبه", "چهارشنبه", "پنجشنبه", "جمعه"
)

val WEEK_DAYS_INITIALS = WEEK_DAYS.map { it.substring(0, 1) }

fun getMonthName(month: Int) = PERSIAN_MONTHS.getOrNull(month - 1) ?: ""

var language = Language.FA
    private set
val mainCalendar inline get() = CalendarType.SHAMSI

var weekStartOffset = 0
    private set
var weekEnds = BooleanArray(7)
    private set
var isTalkBackEnabled = false
    private set
var isHighTextContrastEnabled = false
    private set
var spacedAndInDates = " و "
    private set
var spacedColon = ": "
    private set
var spacedComma = "، "
    private set

// This should be called before any use of Utils on the activity and services
fun initGlobal(context: Context) {
    debugLog("Utils: initGlobal is called")
    updateStoredPreference(context)
}

fun updateStoredPreference(context: Context) {
    debugLog("Utils: updateStoredPreference is called")

    language = Language.preferredDefaultLanguage

    weekStartOffset = language.defaultWeekStart.toIntOrNull() ?: 0

    weekEnds = BooleanArray(7)
    setOf("6").mapNotNull(String::toIntOrNull).forEach { weekEnds[it] = true }

    spacedAndInDates = context.getString(R.string.spaced_and)
    spacedColon = context.getString(R.string.spaced_colon)
    spacedComma = context.getString(R.string.spaced_comma)

    isTalkBackEnabled =
        context.getSystemService<AccessibilityManager>()?.let {
            it.isEnabled && it.isTouchExplorationEnabled
        } ?: false

    // https://stackoverflow.com/a/61599809
    isHighTextContrastEnabled = runCatching {
        context.getSystemService<AccessibilityManager>()?.let {
            it.javaClass.getMethod("isHighTextContrastEnabled").invoke(it) as? Boolean
        }
    }.onFailure(logException).getOrNull() ?: false
}
