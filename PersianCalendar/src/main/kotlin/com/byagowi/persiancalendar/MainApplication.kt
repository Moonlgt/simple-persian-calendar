package com.byagowi.persiancalendar

import android.app.Application

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        initGlobal(applicationContext) // mostly used for things should be provided in locale level
    }
}
