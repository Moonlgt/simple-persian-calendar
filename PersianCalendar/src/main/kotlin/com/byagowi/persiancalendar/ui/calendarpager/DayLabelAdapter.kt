package com.byagowi.persiancalendar.ui.calendarpager

import android.content.Context
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.byagowi.persiancalendar.R
import com.byagowi.persiancalendar.utils.getInitialOfWeekDay
import com.byagowi.persiancalendar.utils.getWeekDayName
import com.byagowi.persiancalendar.utils.revertWeekStartOffsetFromWeekDay
import com.byagowi.persiancalendar.variants.debugAssertNotNull

class DayLabelAdapter(
    private val context: Context,
    val sharedDayViewData: SharedDayViewData,
) : RecyclerView.Adapter<DayLabelAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        DayView(parent.context).also {
            it.layoutParams = sharedDayViewData.layoutParams
            it.sharedDayViewData = sharedDayViewData
        }
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(position)

    override fun getItemCount(): Int = 7

    inner class ViewHolder(itemView: DayView) : RecyclerView.ViewHolder(itemView) {

        fun bind(position: Int) {
            val dayView = (itemView as? DayView).debugAssertNotNull ?: return

            val weekDayPosition = revertWeekStartOffsetFromWeekDay(position)
            dayView.setInitialOfWeekDay(getInitialOfWeekDay(weekDayPosition))
            dayView.contentDescription = context
                .getString(R.string.week_days_name_column, getWeekDayName(weekDayPosition))

            dayView.isVisible = true
            dayView.setBackgroundResource(0)
        }
    }
}
