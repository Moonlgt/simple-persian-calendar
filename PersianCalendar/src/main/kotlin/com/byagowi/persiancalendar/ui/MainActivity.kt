package com.byagowi.persiancalendar.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.byagowi.persiancalendar.databinding.ActivityMainBinding
import com.byagowi.persiancalendar.entities.Theme
import com.byagowi.persiancalendar.initGlobal

/**
 * Program activity for android
 */
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        Theme.apply(this)
        super.onCreate(savedInstanceState)

        initGlobal(this)

        binding = ActivityMainBinding.inflate(layoutInflater).also {
            setContentView(it.root)
        }
    }
}
