package com.byagowi.persiancalendar.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.byagowi.persiancalendar.R
import com.byagowi.persiancalendar.databinding.FragmentMainBinding
import com.byagowi.persiancalendar.navigateSafe

class MainScreen : Fragment(R.layout.fragment_main) {

    private var mainBinding: FragmentMainBinding? = null

    override fun onDestroyView() {
        super.onDestroyView()
        mainBinding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentMainBinding.bind(view)
        mainBinding = binding

        binding.nextMonth.setOnClickListener {
            findNavController().navigateSafe(MainScreenDirections.navigateToBottomSheet())
        }
    }

    //go to selected date
//    private fun bringDate(
//        jdn: Jdn, highlight: Boolean = true, monthChange: Boolean = true,
//        smoothScroll: Boolean = true
//    ) {
//        mainBinding?.calendarPager?.setSelectedDay(jdn, highlight, monthChange, smoothScroll)
//
//        val isToday = Jdn.today() == jdn
//        viewModel.changeSelectedDay(jdn)
//
//        // a11y
//        if (isTalkBackEnabled && !isToday && monthChange) Snackbar.make(
//            mainBinding?.root ?: return,
//            getA11yDaySummary(
//                context ?: return, jdn, false,
//                withZodiac = true, withOtherCalendars = true, withTitle = true
//            ),
//            Snackbar.LENGTH_SHORT
//        ).show()
//    }
}
