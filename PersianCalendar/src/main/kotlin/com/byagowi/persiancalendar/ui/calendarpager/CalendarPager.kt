package com.byagowi.persiancalendar.ui.calendarpager

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.byagowi.persiancalendar.R
import com.byagowi.persiancalendar.databinding.FragmentMonthBinding
import com.byagowi.persiancalendar.entities.Jdn
import com.byagowi.persiancalendar.layoutInflater
import com.byagowi.persiancalendar.mainCalendar
import com.byagowi.persiancalendar.sp
import com.byagowi.persiancalendar.ui.blucalendar.PersianCalendar
import java.lang.ref.WeakReference

class CalendarPager(context: Context, attrs: AttributeSet? = null) : FrameLayout(context, attrs) {

    // Public API
    var onDayClicked = fun(_: Jdn) {}
    var onDayLongClicked = fun(_: Jdn) {}

    // Selected month is visible current month of the pager, maybe a day is not selected on it yet
    var onMonthSelected = fun() {}
    private val baseJdn = Jdn.today()
    val selectedMonth: PersianCalendar
        get() = mainCalendar.getMonthStartFromMonthsDistance(
            baseJdn, -applyOffset(viewPager.currentItem)
        )

    fun setSelectedDay(
        jdn: Jdn,
        highlight: Boolean = true,
        monthChange: Boolean = true,
        smoothScroll: Boolean = true
    ) {
        selectedJdn = if (highlight) jdn else null

        if (monthChange) {
            viewPager.setCurrentItem(
                applyOffset(position = -mainCalendar.getMonthsDistance(baseJdn, jdn)), smoothScroll
            )
        }

        refresh()
    }

    // Public API, to be reviewed
    fun refresh(isEventsModified: Boolean = false) = pagesViewHolders
        .mapNotNull { it.get() }.forEach { it.pageRefresh(isEventsModified, selectedJdn) }

    private val pagesViewHolders = ArrayList<WeakReference<PagerAdapter.ViewHolder>>()

    // Package API, to be rewritten with viewPager.adapter.notifyItemChanged()
    private fun addViewHolder(vh: PagerAdapter.ViewHolder) = pagesViewHolders.add(WeakReference(vh))

    private val monthsLimit = 5000 // this should be an even number

    private fun applyOffset(position: Int) = monthsLimit / 2 - position

    private val viewPager = ViewPager2(context)
    private var selectedJdn: Jdn? = null

    init {
        viewPager.adapter = PagerAdapter()
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) = refresh()
        })
        addView(viewPager)
        viewPager.setCurrentItem(applyOffset(0), false)
    }

    fun goToNextMonth() {
        viewPager.setCurrentItem(viewPager.currentItem + 1, true)
    }

    fun goToNextYear() {
        viewPager.setCurrentItem(viewPager.currentItem + 12, false)
    }

    fun goToPreviousMonth() {
        viewPager.setCurrentItem(viewPager.currentItem - 1, true)
    }

    fun goToPreviousYear() {
        viewPager.setCurrentItem(viewPager.currentItem - 12, false)
    }

    inner class PagerAdapter : RecyclerView.Adapter<PagerAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
            FragmentMonthBinding.inflate(parent.context.layoutInflater, parent, false)
        )

        override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(position)

        override fun getItemCount() = monthsLimit

        private val sharedDayViewData = SharedDayViewData(
            context, resources.getDimension(R.dimen.grid_calendar_height) / 7 - 4.5.sp
        )

        inner class ViewHolder(val binding: FragmentMonthBinding) :
            RecyclerView.ViewHolder(binding.root) {

            var pageRefresh = fun(_: Boolean, _: Jdn?) {}

            init {
                binding.monthView.initialize(sharedDayViewData, this@CalendarPager)

                addViewHolder(this)
            }

            fun bind(position: Int) {
                val monthStartDate = mainCalendar.getMonthStartFromMonthsDistance(
                    baseJdn, -applyOffset(position)
                )
                val monthStartJdn = Jdn(monthStartDate)
                val monthLength =
                    mainCalendar.getMonthLength(
                        monthStartDate.persianYear,
                        monthStartDate.getPersianMonth()
                    )
                binding.monthView.bind(monthStartJdn, monthStartDate)

                pageRefresh = { isEventsModification: Boolean, jdn: Jdn? ->
                    if (viewPager.currentItem == position) {
                        if (isEventsModification && jdn != null) {
                            binding.monthView.initializeMonthEvents()
                            onDayClicked(jdn)
                        } else {
                            onMonthSelected()
                        }

                        binding.monthView.selectDay(
                            if (jdn != null && jdn >= monthStartJdn && jdn - monthStartJdn + 1 <= monthLength)
                                jdn - monthStartJdn + 1
                            else -1
                        )
                    } else binding.monthView.selectDay(-1)
                }

                pageRefresh(false, selectedJdn)
            }
        }
    }
}
