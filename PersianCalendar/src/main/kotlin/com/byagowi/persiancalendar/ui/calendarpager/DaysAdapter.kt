package com.byagowi.persiancalendar.ui.calendarpager

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.byagowi.persiancalendar.entities.Jdn
import com.byagowi.persiancalendar.isTalkBackEnabled
import com.byagowi.persiancalendar.utils.applyWeekStartOffsetToWeekDay
import com.byagowi.persiancalendar.utils.getA11yDaySummary
import com.byagowi.persiancalendar.variants.debugAssertNotNull

class DaysAdapter(
    private val context: Context,
    val sharedDayViewData: SharedDayViewData,
    private val calendarPager: CalendarPager?
) : RecyclerView.Adapter<DaysAdapter.ViewHolder>() {

    var days = emptyList<Jdn>()
    var startingDayOfWeek: Int = 0
    var weekOfYearStart: Int = 0
    var weeksCount: Int = 0

    private var selectedDay = -1

    fun initializeMonthEvents() = Unit

    internal fun selectDay(dayOfMonth: Int) {
        val prevDay = selectedDay
        selectedDay = -1
        notifyItemChanged(prevDay)

        if (dayOfMonth == -1) return

        selectedDay = dayOfMonth + applyWeekStartOffsetToWeekDay(startingDayOfWeek) - 1

        notifyItemChanged(selectedDay)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        DayView(parent.context).also {
            it.layoutParams = sharedDayViewData.layoutParams
            it.sharedDayViewData = sharedDayViewData
        }
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(position)

    // days of week * month view rows
    override fun getItemCount(): Int = 6 * 7

    private val todayJdn = Jdn.today()

    inner class ViewHolder(itemView: DayView) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener, View.OnLongClickListener {

        init {
            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
        }

        override fun onClick(v: View) {
            val itemDayView = (v as? DayView).debugAssertNotNull ?: return
            val jdn = itemDayView.jdn ?: return
            calendarPager?.let { it.onDayClicked(jdn) }
            selectDay(itemDayView.dayOfMonth)
        }

        override fun onLongClick(v: View): Boolean {
            onClick(v)
            val jdn = (v as? DayView).debugAssertNotNull?.jdn ?: return false
            calendarPager?.let { it.onDayLongClicked(jdn) }
            return false
        }

        fun bind(position: Int) {
            val dayView = (itemView as? DayView).debugAssertNotNull ?: return

            val fixedStartingDayOfWeek = applyWeekStartOffsetToWeekDay(startingDayOfWeek)
            if (days.size < position + 2 - fixedStartingDayOfWeek) {
                setEmpty()
            } else {
                if (position + 1 - fixedStartingDayOfWeek >= 0) {
                    val day = days[position + 1 - fixedStartingDayOfWeek]

                    val isToday = day == todayJdn

                    val dayOfMonth = position + 2 - fixedStartingDayOfWeek
                    dayView.setDayOfMonthItem(
                        isToday,
                        position == selectedDay,
                        day,
                        dayOfMonth
                    )

                    dayView.contentDescription =
                        if (isTalkBackEnabled)
                            getA11yDaySummary(
                                context,
                                day,
                                isToday,
                                withTitle = true
                            )
                        else
                            dayOfMonth.toString()

                    dayView.isVisible = true
                } else {
                    setEmpty()
                }
            }
        }

        private fun setEmpty() {
            itemView.isVisible = false
        }
    }
}
