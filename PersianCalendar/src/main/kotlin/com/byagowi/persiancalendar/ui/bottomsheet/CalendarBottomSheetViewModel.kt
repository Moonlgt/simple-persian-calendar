package com.byagowi.persiancalendar.ui.bottomsheet

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.byagowi.persiancalendar.entities.Jdn
import com.byagowi.persiancalendar.mainCalendar
import com.byagowi.persiancalendar.ui.blucalendar.PersianCalendar
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class CalendarBottomSheetViewModel constructor(
    application: Application
) : AndroidViewModel(application) {

    private val _selectedDay = MutableStateFlow(Jdn.today())
    val selectedDay: StateFlow<Jdn> get() = _selectedDay

    private val _selectedMonth = MutableStateFlow(
        mainCalendar.getMonthStartFromMonthsDistance(selectedDay.value, 0)
    )
    val selectedMonth: StateFlow<PersianCalendar> get() = _selectedMonth

    fun changeSelectedMonth(selectedMonth: PersianCalendar) {
        _selectedMonth.value = selectedMonth
    }

    fun changeSelectedDay(jdn: Jdn) {
        _selectedDay.value = jdn
    }
}
