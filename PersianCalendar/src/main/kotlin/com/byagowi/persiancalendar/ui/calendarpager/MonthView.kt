package com.byagowi.persiancalendar.ui.calendarpager

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import androidx.annotation.ColorInt
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.byagowi.persiancalendar.dp
import com.byagowi.persiancalendar.entities.Jdn
import com.byagowi.persiancalendar.getMonthName
import com.byagowi.persiancalendar.language
import com.byagowi.persiancalendar.mainCalendar
import com.byagowi.persiancalendar.ui.blucalendar.PersianCalendar
import com.byagowi.persiancalendar.utils.formatNumber
import kotlin.math.min

class MonthView(context: Context, attrs: AttributeSet? = null) : RecyclerView(context, attrs) {

    init {
        setHasFixedSize(true)
        layoutManager = GridLayoutManager(context, 7)
    }

    private var daysAdapter: DaysAdapter? = null

    fun initialize(sharedDayViewData: SharedDayViewData, calendarPager: CalendarPager) {
        daysAdapter = DaysAdapter(context, sharedDayViewData, calendarPager)
        adapter = daysAdapter
        addCellSpacing(4.dp.toInt())
    }

//    fun initializeForRendering(
//        @ColorInt textColor: Int,
//        width: Int,
//        height: Int,
//        today: AbstractDate
//    ) {
//        val sharedData = SharedDayViewData(
//            context, height / 7f, min(width, height) / 7f, textColor
//        )
//        daysAdapter = DaysAdapter(context, sharedData, null)
//        adapter = daysAdapter
//        val jdn = Jdn(today.year, today.month, 1)
//        bind(jdn, jdn.toCalendar())
//    }

    private fun addCellSpacing(space: Int) {
        addItemDecoration(object : ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect, view: View, parent: RecyclerView, state: State
            ) {
                if (parent.paddingBottom != space) {
                    parent.updatePadding(bottom = space)
                    parent.clipToPadding = false
                }
                outRect.set(0, 0, 0, space)
            }
        })
    }

    private var monthName = ""

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas) // This is important, don't remove it ever

        // Widget only tweak
        val sharedData = daysAdapter?.sharedDayViewData ?: return
        val widgetFooterTextPaint = sharedData.widgetFooterTextPaint ?: return
        canvas.drawText(monthName, width / 2f, height * .95f, widgetFooterTextPaint)
    }

    fun bind(monthStartJdn: Jdn, monthStartDate: PersianCalendar) {
        val monthLength = mainCalendar.getMonthLength(
            monthStartDate.persianYear,
            monthStartDate.getPersianMonth()
        )
        monthName = language.my.format(
            getMonthName(monthStartDate.getPersianMonth()),
            formatNumber(monthStartDate.persianYear)
        )
        contentDescription = monthName

        daysAdapter?.let {
            val startOfYearJdn = Jdn(monthStartDate.persianYear, 1, 1)
            it.startingDayOfWeek = monthStartJdn.dayOfWeek
            it.weekOfYearStart = monthStartJdn.getWeekOfYear(startOfYearJdn)
            it.weeksCount =
                (monthStartJdn + monthLength - 1).getWeekOfYear(startOfYearJdn) - it.weekOfYearStart
            it.days = monthStartJdn.createMonthDaysList(monthLength)
            it.initializeMonthEvents()
            it.notifyItemRangeChanged(0, it.itemCount)
        }
    }

    fun initializeMonthEvents() {
        daysAdapter?.initializeMonthEvents()
    }

    fun selectDay(dayOfMonth: Int) {
        daysAdapter?.selectDay(dayOfMonth)
    }
}
