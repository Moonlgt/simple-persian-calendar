/**
 * Persian Calendar see: http://code.google.com/p/persian-calendar/
 * Copyright (C) 2012  Mortezaadi@gmail.com
 * PersianCalendar.java
 *
 *
 * Persian Calendar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http:></http:>//www.gnu.org/licenses/>.
 */
package com.byagowi.persiancalendar.ui.blucalendar

import android.text.format.DateUtils
import ir.huri.jcal.JalaliCalendar
import java.util.Calendar
import java.util.Date
import java.util.GregorianCalendar
import java.util.TimeZone

/**
 * ** Persian(Shamsi) calendar **
 *
 *
 *
 *
 *
 * The calendar consists of 12 months, the first six of which are 31 days, the
 * next five 30 days, and the final month 29 days in a normal year and 30 days
 * in a leap year.
 *
 *
 *
 * As one of the few calendars designed in the era of accurate positional
 * astronomy, the Persian calendar uses a very complex leap year structure which
 * makes it the most accurate solar calendar in use today. Years are grouped
 * into cycles which begin with four normal years after which every fourth
 * subsequent year in the cycle is a leap year. Cycles are grouped into grand
 * cycles of either 128 years (composed of cycles of 29, 33, 33, and 33 years)
 * or 132 years, containing cycles of of 29, 33, 33, and 37 years. A great grand
 * cycle is composed of 21 consecutive 128 year grand cycles and a final 132
 * grand cycle, for a total of 2820 years. The pattern of normal and leap years
 * which began in 1925 will not repeat until the year 4745!
 *
 *  Each 2820 year great grand cycle contains 2137 normal years of 365 days
 * and 683 leap years of 366 days, with the average year length over the great
 * grand cycle of 365.24219852. So close is this to the actual solar tropical
 * year of 365.24219878 days that the Persian calendar accumulates an error of
 * one day only every 3.8 million years. As a purely solar calendar, months are
 * not synchronized with the phases of the Moon.
 *
 *
 *
 *
 *
 *
 *
 * **PersianCalendar** by extending Default GregorianCalendar
 * provides capabilities such as:
 *
 *
 *
 *
 *
 *
 *  * you can set the date in Persian by setPersianDate(persianYear,
 * persianMonth, persianDay) and get the Gregorian date or vice versa
 *
 *
 *
 *  * determine is the current date is Leap year in persian calendar or not by
 * IsPersianLeapYear()
 *
 *
 *
 *  * getPersian short and long Date String getPersianShortDate() and
 * getPersianLongDate you also can set delimiter to assign delimiter of returned
 * dateString
 *
 *
 *
 *  * Parse string based on assigned delimiter
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * ** Example **
 *
 *
 *
 *
 *
 *
 * <pre>
 * `PersianCalendar persianCal = new PersianCalendar();
 * System.out.println(persianCal.getPersianShortDate());

 * persianCal.set(1982, Calendar.MAY, 22);
 * System.out.println(persianCal.getPersianShortDate());

 * persianCal.setDelimiter(" , ");
 * persianCal.parse("1361 , 03 , 01");
 * System.out.println(persianCal.getPersianShortDate());

 * persianCal.setPersianDate(1361, 3, 1);
 * System.out.println(persianCal.getPersianLongDate());
 * System.out.println(persianCal.getDate());

 * persianCal.addPersianDate(Calendar.MONTH, 33);
 * persianCal.addPersianDate(Calendar.YEAR, 5);
 * persianCal.addPersianDate(Calendar.DATE, 50);

` *

 * <pre>
 * @author Morteza  contact: [Mortezaadi@gmail.com](mailto:Mortezaadi@gmail.com)
 * *
 * @version 1.1
</pre></pre> */
class PersianCalendar : GregorianCalendar {

    var persianYear: Int = 0
        private set
    private var persianMonth: Int = 0

    /**
     * @return int Persian day in month
     */
    var persianDay: Int = 0
        private set
    // use to seperate PersianDate's field and also Parse the DateString based
    // on this delimiter
    /**
     * assign delimiter to use as a separator of date fields.

     * @param delimiter
     */
    var delimiter = "/"

    /**
     * default constructor
     *
     *
     * most of the time we don't care about TimeZone when we persisting Date or
     * doing some calculation on date. ** Default TimeZone was set to
     * "GMT" ** in order to make developer to work more convenient with
     * the library; however you can change the TimeZone as you do in
     * GregorianCalendar by calling setTimeZone()
     */
    constructor(millis: Long) {
        timeInMillis = millis
    }

    /**
     * default constructor
     *
     *
     * most of the time we don't care about TimeZone when we persisting Date or
     * doing some calculation on date. ** Default TimeZone was set to
     * "GMT" ** in order to make developer to work more convenient with
     * the library; however you can change the TimeZone as you do in
     * GregorianCalendar by calling setTimeZone()
     */
    constructor() {
        timeZone = TimeZone.getTimeZone("Iran")
    }

    override fun setTimeZone(zone: TimeZone) {
        super.setTimeZone(zone)
        calculatePersianDate()
    }

    override fun setTimeInMillis(millis: Long) {
        super.setTimeInMillis(millis)
        calculatePersianDate()
    }

    override fun set(field: Int, value: Int) {
        super.set(field, value)
        calculatePersianDate()
    }

    override fun toString(): String {
        val str = super.toString()
        return str.substring(0, str.length - 1) + ",PersianDate=" + persianShortDate + "]"
    }

    private fun convertToMilis(julianDate: Long): Long =
        PersianCalendarConstants.MILLIS_JULIAN_EPOCH
            .plus(julianDate * PersianCalendarConstants.MILLIS_OF_A_DAY)
            .plus(
                PersianCalendarUtils.ceil(
                    (timeInMillis - PersianCalendarConstants.MILLIS_JULIAN_EPOCH).toDouble(),
                    PersianCalendarConstants.MILLIS_OF_A_DAY.toDouble()
                )
            )

    /**
     * Calculate persian date from current Date and populates the corresponding
     * fields(persianYear, persianMonth, persianDay)
     */
    private fun calculatePersianDate() {
        val jalaliCalendar = JalaliCalendar(Date(timeInMillis))
        persianYear = jalaliCalendar.year
        persianMonth = jalaliCalendar.month - 1
        persianDay = jalaliCalendar.day
    }

    fun getPersianMonthsTillNow(fromDate: Long): ArrayList<String> {
        val tempTimeMillis = timeInMillis
        val dates = ArrayList<String>()
        val nowMonth = persianYear * 12 + persianMonth
        timeInMillis = fromDate
        val createdMonth = persianYear * 12 + persianMonth
        var monthCount = nowMonth - createdMonth + 1
        while (monthCount > 0) {
            dates.add("$persianMonthName $persianYear")
            addPersianDate(Calendar.MONTH, 1)
            monthCount--
        }
        dates.reverse()
        timeInMillis = tempTimeMillis
        return dates
    }

    fun getPersianDatesTillNow(fromDate: Long): ArrayList<SunDate> {
        val tempTimeMillis = timeInMillis
        val dates = ArrayList<SunDate>()
        val nowMonth = persianYear * 12 + persianMonth
        timeInMillis = fromDate
        val createdMonth = persianYear * 12 + persianMonth
        var monthCount = nowMonth - createdMonth + 1
        while (monthCount > 0) {
            dates.add(
                SunDate(
                    persianYear,
                    persianMonthName,
                    persianMonthNameWithYear(),
                    timeInMillis,
                    persianMonth
                )
            )
            addPersianDate(Calendar.MONTH, 1)
            monthCount--
        }
        dates.reverse()
        timeInMillis = tempTimeMillis
        return dates
    }

    fun getDatesTillNow(fromDate: Long): ArrayList<java.util.Date> {
        val dates: ArrayList<java.util.Date> = ArrayList()
        val tempTimeMillis = timeInMillis
        val calendar = PersianCalendar(fromDate).apply {
            set(Calendar.HOUR_OF_DAY, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
        var startTime: Long = calendar.timeInMillis
        while (startTime <= tempTimeMillis) {
            dates.add(Date(startTime))
            startTime += DateUtils.DAY_IN_MILLIS
        }
        timeInMillis = tempTimeMillis
        return dates
    }

    fun getDatesTillDate(fromDate: Long, toDate: Long): ArrayList<java.util.Date> {
        val dates: ArrayList<java.util.Date> = ArrayList()
        val calendar = PersianCalendar(fromDate).apply {
            set(Calendar.HOUR_OF_DAY, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
        var startTime: Long = calendar.timeInMillis
        while (startTime <= toDate) {
            dates.add(Date(startTime))
            startTime += DateUtils.DAY_IN_MILLIS
        }
        return dates
    }

    fun getNextPersianDates(fromNow: Long, count: Int): ArrayList<SunDate> {
        val tempTimeMillis = timeInMillis
        val dates = ArrayList<SunDate>()
        timeInMillis = fromNow
        for (i in 0 until count) {
            addPersianDate(Calendar.MONTH, 1)
            dates.add(
                SunDate(
                    persianYear,
                    persianMonthName,
                    persianMonthNameWithYear(),
                    timeInMillis,
                    persianMonth,
                    MonthType.AFTER_OPENING
                )
            )
        }
        dates.reverse()
        timeInMillis = tempTimeMillis
        return dates
    }

    fun persianMonthNameWithYear(force: Boolean = false): String {
        return if (persianMonth == 0 || force) {
            ""
//            String.format(null, "%s %s", persianMonthName, persianYear.toPersianNumbers())
        } else {
            persianMonthName
        }
    }

    fun getPreviousPersianDates(fromNow: Long, count: Int): ArrayList<SunDate> {
        val tempTimeMillis = timeInMillis
        val dates = ArrayList<SunDate>()
        timeInMillis = fromNow
        for (i in 0 until count) {
            addPersianDate(Calendar.MONTH, -1)
            dates.add(
                SunDate(
                    persianYear,
                    persianMonthName,
                    persianMonthNameWithYear(),
                    timeInMillis,
                    persianMonth,
                    MonthType.BEFORE_OPENING
                )
            )
        }
        timeInMillis = tempTimeMillis
        return dates
    }

    /**
     * Determines if the given year is a leap year in persian calendar. Returns
     * true if the given year is a leap year.

     * @return boolean
     */
    val isPersianLeapYear: Boolean
        get() = PersianCalendarUtils.isPersianLeapYear(this.persianYear)

    /**
     * set the persian date it converts PersianDate to the Julian and assigned
     * equivalent milliseconds to the instance

     * @param persianYear
     * *
     * @param persianMonth
     * *
     * @param persianDay
     */
    fun setPersianDate(persianYear: Int, persianMonth: Int, persianDay: Int) {
        this.persianYear = persianYear
        this.persianMonth = persianMonth
        this.persianDay = persianDay
        timeInMillis = convertToMilis(
            PersianCalendarUtils.persianToJulian(
                (if (this.persianYear > 0) this.persianYear else this.persianYear + 1).toLong(),
                this.persianMonth - 1,
                this.persianDay
            )
        )
    }

    /**
     * @return int persian month number
     */
    fun getPersianMonth(): Int = // calculatePersianDate();
        this.persianMonth + 1

    /**
     * @return String persian month name
     */
    // calculatePersianDate();
    val persianMonthName: String
        get() = PersianCalendarConstants.persianMonthNames[this.persianMonth]

    /**
     * @return String of persian date formatted by
     * * 'YYYY[delimiter]mm[delimiter]dd' default delimiter is '/'
     */
    val persianShortDate: String
        get() = "" + formatToMilitary(this.persianYear) + delimiter + formatToMilitary(
            getPersianMonth()
        ) + delimiter + formatToMilitary(this.persianDay)

    private fun formatToMilitary(i: Int): String = if (i < 10) "0$i" else i.toString()

    /**
     * add specific amout of fields to the current date for now doesnt handle
     * before 1 farvardin hejri (before epoch)

     * @param field
     * *
     * @param amount <pre>
     * *                              Usage:
     * *                              `addPersianDate(Calendar.YEAR, 2);
     * addPersianDate(Calendar.MONTH, 3);
     ` *
     * *                             </pre>
     * *
     *
     *
     * *               u can also use Calendar.HOUR_OF_DAY,Calendar.MINUTE,
     * *               Calendar.SECOND, Calendar.MILLISECOND etc
     */
    //
    fun addPersianDate(field: Int, amount: Int) {
        when {
            amount == 0 -> return // Do nothing!
            field < 0 || field >= Calendar.ZONE_OFFSET -> throw IllegalArgumentException()
            field == Calendar.YEAR -> {
                setPersianDate(this.persianYear + amount, getPersianMonth(), this.persianDay)
                return
            }
            field == Calendar.MONTH -> {
                setPersianDate(
                    this.persianYear + (getPersianMonth() + amount) / 12,
                    (getPersianMonth() + amount) % 12,
                    28
                )
                return
            }
            else -> {
                add(field, amount)
                calculatePersianDate()
            }
        }
    }

    /**
     * <pre>
     * use `[PersianDateParser]` to parse string
     * and get the Persian Date.
     </pre> *

     * @param dateString
     * *
     * @see PersianDateParser
     */
    fun parse(dateString: String) {
        val p = PersianDateParser(dateString, delimiter).persianDate
        setPersianDate(p.persianYear, p.persianMonth, p.persianDay)
    }

    companion object {
        private val serialVersionUID = 5541422440580682494L
    }
}
