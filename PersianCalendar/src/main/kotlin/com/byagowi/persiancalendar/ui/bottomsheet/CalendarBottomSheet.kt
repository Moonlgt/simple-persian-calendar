package com.byagowi.persiancalendar.ui.bottomsheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.byagowi.persiancalendar.R
import com.byagowi.persiancalendar.databinding.BottomSheetCalendarBinding
import com.byagowi.persiancalendar.entities.Jdn
import com.byagowi.persiancalendar.getMonthName
import com.byagowi.persiancalendar.sp
import com.byagowi.persiancalendar.ui.calendarpager.SharedDayViewData
import com.byagowi.persiancalendar.utils.formatNumber
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class CalendarBottomSheet : BottomSheetDialogFragment(R.layout.bottom_sheet_calendar) {

    private val viewModel by viewModels<CalendarBottomSheetViewModel>()
    private var mainBinding: BottomSheetCalendarBinding? = null

    private val sharedDayViewData by lazy {
        SharedDayViewData(
            requireContext(),
            resources.getDimension(R.dimen.grid_calendar_height) / 7 - 4.5.sp
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) =
        super.onCreateView(inflater, container, savedInstanceState).apply {
            this?.layoutDirection = View.LAYOUT_DIRECTION_RTL
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = BottomSheetCalendarBinding.bind(view)
        mainBinding = binding

        binding.previousMonth.let {
//            it.contentDescription = it.context.getString(
//                R.string.previous_x, it.context.getString(R.string.month)
//            )
            it.setOnClickListener { binding.calendarPager.goToPreviousMonth() }
            it.setOnLongClickListener {
                binding.calendarPager.goToPreviousYear()
                true
            }
        }

        binding.nextMonth.let {
//            it.contentDescription = it.context.getString(
//                R.string.next_x, it.context.getString(R.string.month)
//            )
            it.setOnClickListener { binding.calendarPager.goToNextMonth() }
            it.setOnLongClickListener {
                binding.calendarPager.goToNextYear()
                true
            }
        }

        binding.dayLabelView.initialize(sharedDayViewData)

        binding.calendarPager.also {
            it.onDayClicked = { jdn -> bringDate(jdn, monthChange = false) }
            it.setSelectedDay(
                Jdn(viewModel.selectedMonth.value), highlight = false, smoothScroll = false
            )
            it.onMonthSelected = { viewModel.changeSelectedMonth(it.selectedMonth) }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.selectedMonth
                .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
                .collectLatest { date ->
                    binding.yearMonth.text =
                        getMonthName(date.getPersianMonth()).plus(formatNumber(date.persianYear))
                }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mainBinding = null
    }

    private fun bringDate(
        jdn: Jdn,
        highlight: Boolean = true,
        monthChange: Boolean = true,
        smoothScroll: Boolean = true
    ) {
        mainBinding?.calendarPager?.setSelectedDay(jdn, highlight, monthChange, smoothScroll)

        viewModel.changeSelectedDay(jdn)
    }
}
