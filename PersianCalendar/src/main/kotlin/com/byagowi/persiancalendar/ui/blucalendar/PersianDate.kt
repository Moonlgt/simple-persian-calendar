package com.byagowi.persiancalendar.ui.blucalendar

import java.io.Serializable

/**
 * Besm e ALLAH e Rahman e Rahim
 * Project: BlueBank
 * Created by Hossein Kheirollah Pour on 2020 - 05 - 04
 * Telegram: @HKH114
 */
data class PersianDate(
    var yyyy: Int,
    var month: Int,
    var day: Int
) : Serializable {
    val yy: Int
        get() = yyyy.toString().substring(2).toInt()
}

data class SunDate(
    val year: Int,
    val month: String,
    val monthByYear: String,
    val time: Long,
    val monthNumber: Int,
    var type: MonthType = MonthType.ORIGINAL
) {
    override fun equals(other: Any?): Boolean {
        (other as? SunDate)?.let {
            if (year == other.year && monthNumber == other.monthNumber)
                return true
        }
        return false
    }

    override fun hashCode(): Int {
        var result = year
        result = 31 * result + month.hashCode()
        result = 31 * result + time.hashCode()
        result = 31 * result + monthNumber
        result = 31 * result + type.hashCode()
        return result
    }
}

enum class MonthType {
    BEFORE_OPENING, AFTER_OPENING, ORIGINAL;

    fun isDisabled() = this == BEFORE_OPENING || this == AFTER_OPENING
}

fun SunDate.toPersianDate() = PersianDate(
    year, monthNumber, 0
)
