package com.byagowi.persiancalendar.ui.calendarpager

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import androidx.annotation.ColorInt
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.byagowi.persiancalendar.dp
import kotlin.math.min

class DayLabelView(context: Context, attrs: AttributeSet? = null) :
    RecyclerView(context, attrs) {

    init {
        setHasFixedSize(true)
        layoutManager = GridLayoutManager(context, 7)
    }

    private var daysAdapter: DayLabelAdapter? = null

    fun initialize(sharedDayViewData: SharedDayViewData) {
        daysAdapter = DayLabelAdapter(context, sharedDayViewData)
        adapter = daysAdapter
        addCellSpacing(4.dp.toInt())
    }

//    fun initializeForRendering(
//        @ColorInt textColor: Int,
//        width: Int,
//        height: Int,
//        today: AbstractDate
//    ) {
//        val sharedData = SharedDayViewData(
//            context, height / 7f, min(width, height) / 7f, textColor
//        )
//        daysAdapter = DayLabelAdapter(context, sharedData)
//        adapter = daysAdapter
//        bind()
//    }

    private fun addCellSpacing(space: Int) {
        addItemDecoration(object : ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect, view: View, parent: RecyclerView, state: State
            ) {
                if (parent.paddingBottom != space) {
                    parent.updatePadding(bottom = space)
                    parent.clipToPadding = false
                }
                outRect.set(0, 0, 0, space)
            }
        })
    }

    private var monthName = ""

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas) // This is important, don't remove it ever

        // Widget only tweak
        val sharedData = daysAdapter?.sharedDayViewData ?: return
        val widgetFooterTextPaint = sharedData.widgetFooterTextPaint ?: return
        canvas.drawText(monthName, width / 2f, height * .95f, widgetFooterTextPaint)
    }

    fun bind() {
        daysAdapter?.let {
            it.notifyItemRangeChanged(0, it.itemCount)
        }
    }
}
