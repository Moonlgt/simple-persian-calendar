package com.byagowi.persiancalendar

import android.content.Context
import android.content.ContextWrapper
import android.content.res.Resources
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import com.byagowi.persiancalendar.variants.debugAssertNotNull

val Number.dp: Float get() = this.toFloat() * Resources.getSystem().displayMetrics.density
val Number.sp: Float get() = this.toFloat() * Resources.getSystem().displayMetrics.scaledDensity

val Context.layoutInflater: LayoutInflater get() = LayoutInflater.from(this)

// https://stackoverflow.com/a/58249983
private tailrec fun Context.getActivity(): FragmentActivity? = this as? FragmentActivity
    ?: (this as? ContextWrapper)?.baseContext?.getActivity()

@ColorInt
fun Context.resolveColor(@AttrRes attribute: Int) = TypedValue().let {
    theme.resolveAttribute(attribute, it, true)
    ContextCompat.getColor(this, it.resourceId)
}

fun NavController.navigateSafe(directions: NavDirections) = runCatching {
    navigate(directions)
}.onFailure(logException).getOrNull().debugAssertNotNull.let {}

val logException = fun(e: Throwable) { Log.e(LOG_TAG, "Handled Exception", e) }
